# Script for polling the translink API for bus locations
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# NOTE: Register for a translink api key in order to use this script

from google.transit import gtfs_realtime_pb2
import requests
import json, csv, time

OUTPUT_FILE = "output.csv"
API_KEY = "your_api_key_here"
outfile = open(OUTPUT_FILE, "a")
csvOut = csv.writer(outfile)
csvOut.writerow(["vehicle_id", "timestamp", "lat", "lon"])
feed = gtfs_realtime_pb2.FeedMessage()

while(True):
    response = requests.get('https://gtfs.translink.ca/v2/gtfsposition?apikey=' + API_KEY)
    feed.ParseFromString(response.content)
    for entity in feed.entity:
        entity = entity.vehicle
        csvOut.writerow([entity.vehicle.id, entity.timestamp, entity.position.latitude, entity.position.longitude] )     
    time.sleep(60)