# Map of Metro Vancouver from Bus Tracking Data

## TransLink API

In order to use the data collection script in this repository, you must first register an account with the [TransLink API](https://developer.translink.ca/Account/Register)

## Final Result

![final_result](./result.gif)

## Purpose of this project

This project was merely meant to serve as a demonstration of the TransLink realtime tracking and API. As well as serving as a visual representation of the public transport network in Vancouver, CA.

## Process

1. Data was pulled from the translink API using python3. In order to get realtime data, it was necessary to use Google's protobuf protocol. 
   - For this it is possible to use either the python package `gtfs_realtime_pb2` or compile the `gtfs-realtime.proto` file in this repository.
2. The resulting data was parsed in python, with each 'frame' containing data from a particular time slot.
    - Frames were then rendered using `pyplot`
3. The graphs generated by pyplot were then loaded into blender where they were rendered into a set of frames (also where the title and python logo were added)
4. ffmpeg was used to generate the final result