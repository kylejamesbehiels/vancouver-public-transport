# Data processing script for parsing the vancouver bus data and generating graphs
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# NOTE: This code is far from efficient and of a relatively low quality,
# while it works, optimization can (and might eventually) be done to improve
# efficiency and readability

import csv
import matplotlib.pyplot as plt
from tqdm import tqdm
import arrow

READ_FILE = "output.csv"
# Low value for testing
TOTAL_FRAMES = 600

print("Reading and parsing data...")

infile = open(READ_FILE, "r")
incsv = csv.reader(infile)

fields = incsv.__next__()
data = []
timestamps = []
lats = []
lons = []
for point in incsv:
    if(point == fields):
        continue
    if(float(point[2]) == 0 or float(point[3]) == 0):
        continue
    data.append(point)
    timestamps.append(point[1])
    lats.append(float(point[2]))
    lons.append(float(point[3]))
frames = []
secondsPerFrame = (float(max(timestamps)) - float(min(timestamps)))/TOTAL_FRAMES
curFrameTime = float(min(timestamps))

minLats = min(lats)
minLons = min(lons)
maxLats = max(lats)
maxLons = max(lons)

print("Lats Range " + str(minLats) + " to " + str(maxLats))
print("Lons Range " + str(minLons) + " to " + str(maxLons))
print("Using time frames of " + str(secondsPerFrame) + " seconds per actual frame.")

progress = 0

frameCount = 0
includedDataPoints = 0
print("Generating frames...")
with tqdm(total=TOTAL_FRAMES) as pbar:
    while(curFrameTime < int(max(timestamps))):
        pbar.update(frameCount)
        frameCount+=1
        currentFrame = {}
        for point in data:
            if(float(point[2]) == 0 or float(point[3]) == 0):
                continue

            # if(float(point[1]) <= (curFrameTime + secondsPerFrame) and float(point[1]) >= curFrameTime):
            if(float(point[1]) <= (curFrameTime + secondsPerFrame)):
                includedDataPoints += 1
                # Data point should be included in current frame
                try:
                    currentFrame[point[0]].append([float(point[2]), float(point[3])])
                except:
                    currentFrame[point[0]] = []
                    currentFrame[point[0]].append([float(point[2]), float(point[3])])
        frames.append(currentFrame)
        curFrameTime += secondsPerFrame
print("Included " + str(includedDataPoints) + " out of " + str(len(data)))
print("Generating graphs from frames...")
frameIndex = 0


curFrameTime = int(min(timestamps))
for frame in tqdm(frames[0:300]):

    utc = arrow.Arrow.fromtimestamp(curFrameTime)
    conv = utc.to('America/Vancouver')
    outTime = conv.format("h:mmA ") + conv.tzname()

    plt.clf()
    plt.xlim(minLats, maxLats)
    plt.ylim(minLons, maxLons)
    for vehicle in frame:
        vehicleLineX = []
        vehicleLineY = []
        for point in frame[vehicle]:
            # print(point)
            vehicleLineX.append(point[0])
            vehicleLineY.append(point[1])
        plt.xticks(vehicleLineX, "")
        plt.yticks(vehicleLineY, "")
        plt.xlabel(str(outTime))
        
        plt.plot(vehicleLineX, vehicleLineY, ('black'), linewidth=1.0)

    plt.savefig("frames/" + str(frameIndex)+".png", dpi=220)
    frameIndex += 1
    curFrameTime += secondsPerFrame






